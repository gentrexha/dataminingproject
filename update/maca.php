<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 6/1/2017
 * Time: 15:26
 */

header("Content-Type: application/json");



require('config.php');
# fb-initialization
require_once __DIR__ . '/../facebook-sdk-v5/autoload.php';
if(!session_id()) {
    session_start();
}

$fb = new Facebook\Facebook([
    'app_id' => '1361304643927217',
    'app_secret' => '3ad8e25a982aff16c1ff87b1a1da2f5f',
    'default_graph_version' => 'v2.10'
]);

// Getting long-lived token.
$token = $longtoken;
$fb->setDefaultAccessToken($token);

// Removing time limit because reponses take longer than 30 seconds, which is default.
set_time_limit(0);

# db_Connection-initialization
$config = parse_ini_file('../secure/db_config.ini');
$dbConn = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
if (!$dbConn)
{
    die('Connection to database couldnt be established: '.mysqli_connect_error());
}
else
{
    echo 'Connected to '.$config['host'].' succesfully!';
}



function Prova($pageName, $fb, $token, $dbConn)
{
    try {
        $dayseconds=strtotime("-15 Days");
        $day= date("d M Y", $dayseconds);
		//$query=$pageName."/feed?fields=id,likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares&until=now&since=$day";
		//$query=$pageName."/feed?fields=id,likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares&until=6 July 2014&since=6 May 2014";
       //	$query=$pageName."/feed?fields=id,reactions.limit(1).summary(total_count),likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares&until=now&since=$day";
	   $query=$pageName."/?fields=posts.since(7 October 2017).until(23 October 2017){id,reactions.limit(1).summary(total_count),likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares}";

	   
	   
	   $response = $fb->get($query, $token);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }
    $feedEdge = $response->getBody();
    $jsonData = json_decode($response->getBody());
    $data = $jsonData->posts->data;
    foreach($data as $nrPosteve)
    {
        $KohaPostimit1=$nrPosteve->created_time;
        $KohaPostimit = date("Y-m-d H:i:s", strtotime($KohaPostimit1));
        $IdPostimit=$nrPosteve->id;
        $NrKomenteve=$nrPosteve->comments->summary->total_count;
        $NrLikeve=$nrPosteve->likes->summary->total_count;
		$NrShares="";
		$nrReactions=$nrPosteve->reactions->summary->total_count;
		//echo $pageName." ".$nrReactions." ";
		
		if($pageName == 'ShpendAhmetii' || $pageName == 'CaushiDriton' || $pageName == 'KryetarFehmiFerati' || $pageName == 'FatonTopalli2017' || $pageName == 'SamiKurteshi13' || $pageName == 'mhaskukaa' || $pageName == 'bnurboja' || $pageName=='QendronKastrati10' || $pageName=='AjetPotera2017' || $pageName=='310775596061589' || $pageName=='VVMalisheva' || $pageName=='salihsalihu19'|| $pageName=='zekesinanajdecan'|| $pageName=='Agron.Avdijaj10'|| $pageName=='LvvKacanik'|| $pageName=='LVVKline'|| $pageName=='221283037890885'|| $pageName=='SelatinRetkoceriVV'|| $pageName=='vetevendosjekastriot' || $pageName=='1934481086825941')
		{			
			$partia='Lëvizja VETËVENDOSJE!';
		}
		else if ($pageName=='abrashiarban' || $pageName == 'safetkamberipermitrovicen' || $pageName == 'muharremsvarqazyrtare' || $pageName == 'hazirilutfi' || $pageName == 'DrHatimBaxhaku' || $pageName == 'gazmuhaxheri' || $pageName=='ShaipSurdulliKamenice'|| $pageName=='voto47'|| $pageName=='selimshabanldk47'|| $pageName=='asllanajsali'|| $pageName=='sokolihaliti.official'|| $pageName=='xhafertahiriofficial'|| $pageName=='ldkjunikfaqjazyrtare' || $pageName=='AgimVeliu'|| $pageName=='mal.lokaj1'|| $pageName=='910656602314125'|| $pageName=='505468136208097'|| $pageName=='ShkelqimMJakupi'|| $pageName=='besimhotikline'|| $pageName=='BurimBerishaOfficial'|| $pageName=='imri.ahmeti'|| $pageName=='bajrushymeri'|| $pageName=='101664896548627')
		{
			$partia='LDK';

		}
		else if ($pageName=='selimpacolliofficial' || $pageName == 'Agim.Bahtirii' || $pageName=='1471112073125298')
		{
			$partia='AKR';
		}
		else if ($pageName=='LirakCelaj.ks' || $pageName == 'RamadanHotii' || $pageName == 'validrizi' || $pageName == 'AgimAliuPDK94' || $pageName == 'ZenunPajazitiOfficial' || $pageName == 'shaqirtotaj' || $pageName =='sabihashalaofficial' || $pageName=='Dr.IdrizVehapi' || $pageName=='Dr.IdrizVehapi' || $pageName=='491857897815166' || $pageName=='pdknaimismajli' || $pageName=='BlerimiperSuhareke' || $pageName=='pdkdegaviti' || $pageName=='feritidrizii' || $pageName=='pdkmalisheva'|| $pageName=='BlerandKadrijajofficial'|| $pageName=='ramizlladrovci'|| $pageName=='ProfDrBeqirSadikaj'|| $pageName=='BesimIlaziPDK'|| $pageName=='BashotaSokol'|| $pageName=='548957518774900' || $pageName=='IsmetHajrullahuPDK'|| $pageName=='1940058396319659')
		{
			$partia='PDK';
		}
		else if ($pageName=='vllahiuarber' || $pageName == 'ardiangjiniaak' || $pageName == 'XhavitZariqiOfficial' || $pageName == 'RexhepKadriu.rks' || $pageName == 'lulzimkabashi.ak' || $pageName == 'fatmirgashipeje' || $pageName == 'SmajlLatifi50' || $pageName=='aaksuharekeofficial' || $pageName=='222542211233814'|| $pageName=='1891305244219623'|| $pageName=='1555763194717660'|| $pageName=='HajrushKurtajOfficial'|| $pageName=='Zenunelezaj1963'|| $pageName=='XhaferGashiPerObiliq')
		{
			$partia='AAK';
		}
		else if($pageName=='mimozakusari')
		{
		$partia="ALT";
		}
		else if($pageName=='BekimHamezJashari' || $pageName=='FatmirRashitiVoto54')
		{
			$partia="KP";
		}
		else if ($pageName=='strpce.ldp.9' || $pageName=='SrdjanPopovicZaGradonacelnika' || $pageName=='1914605832085405' || $pageName=='130664660988134')
		{
			$partia="SL";
		}
		else if($pageName=='oliverivanovickos')
		{
			$partia='IQ';
		}
		else if($pageName=='KTAP.KOSOVA')
		{
			$partia='KTAP';
		}
		else if($pageName=='kdtp.rks')
		{
			$partia='KDTP';
		}
		else if($pageName=='xhemajlajisa' || $pageName=='533546846720711')
		{
			$partia='NISMA';
		}
		else if($pageName=='gradonacelnikjablanovic')
		{
			$partia='PKS';
		}
		
        if(!isset($nrPosteve->shares->count))
        {
            $NrShares="0";
        }
        else
        {
            $NrShares=$nrPosteve->shares->count;
        }
        $sqlQuery = "insert into postimetekryetarevekomunave(postiid,EmriKandidatit,EmriPartise,kohapostimit,nrLike,nrReaction,nrKomenteve,nrShare) VALUES ('$IdPostimit', '$pageName','$partia', '$KohaPostimit','$NrLikeve','$nrReactions','$NrKomenteve','$NrShares')
		ON DUPLICATE KEY UPDATE
		EmriKandidatit='$pageName',EmriPartise='$partia',kohapostimit='$KohaPostimit',nrlike='$NrLikeve',nrReaction='$nrReactions',nrkomenteve='$NrKomenteve',nrshare='$NrShares'";
        $dbResponse = mysqli_query($dbConn , $sqlQuery);

    }
    
	
    
    if(!isset($jsonData->posts->paging->next))
    {
        $i=false;
    }
    else
    {
        $i=true;
		$nextpage=$jsonData->posts->paging->next;
    }
    while($i==true)
    {
        // Nese ndron verzioni ndro explode
        $nextquery=explode('https://graph.facebook.com/v2.10',$nextpage);

        $query2Page=$nextquery[1];
        $response = $fb->get($query2Page, $token);
        $feedEdge = $response->getBody();
        $jsonData = json_decode($response->getBody());
        $data = $jsonData->data;
        foreach($data as $nrPosteve)
        {
            $KohaPostimit1=$nrPosteve->created_time;
            $KohaPostimit = date("Y-m-d H:i:s", strtotime($KohaPostimit1));
            $IdPostimit=$nrPosteve->id;
            $NrKomenteve=$nrPosteve->comments->summary->total_count;
            $NrLikeve=$nrPosteve->likes->summary->total_count;
			$nrReactions=$nrPosteve->reactions->summary->total_count;
			$NrShares="";
            if(!isset($nrPosteve->shares->count))
            {
                $NrShares="0";
            }
            else
            {
                $NrShares=$nrPosteve->shares->count;
            }
            $sqlQuery = "insert into postimetekryetarevekomunave(postiid,EmriKandidatit,EmriPartise,kohapostimit,nrLike,nrReaction,nrKomenteve,nrShare) VALUES ('$IdPostimit', '$pageName','$partia', '$KohaPostimit','$NrLikeve','$nrReactions','$NrKomenteve','$NrShares')
		ON DUPLICATE KEY UPDATE
		EmriKandidatit='$pageName',EmriPartise='$partia',kohapostimit='$KohaPostimit',nrlike='$NrLikeve',nrReaction='$nrReactions',nrkomenteve='$NrKomenteve',nrshare='$NrShares'";
            $dbResponse = mysqli_query($dbConn , $sqlQuery);
			
        }
         if(!isset($jsonData->paging->next))
        {
            $i=false;
        }
		else
		{
			$nextpage=$jsonData->paging->next;
       
		}
    }
}


//'ShpendAhmetii','abrashiarban','selimpacolliofficial','LirakCelaj.ks','vllahiuarber','CaushiDriton','ardiangjiniaak','mimozakusari','RamadanHotii','KryetarFehmiFerati','Agim.Bahtirii','validrizi'

//'491857897815166','310775596061589','533546846720711','222542211233814','1914605832085405','130664660988134','1891305244219623','910656602314125'

//'505468136208097','1555763194717660','548957518774900','221283037890885','101664896548627','1940058396319659','1471112073125298','1934481086825941'

//'safetkamberipermitrovicen','FatonTopalli2017','muharremsvarqazyrtare','XhavitZariqiOfficial','AgimAliuPDK94','SamiKurteshi13','hazirilutfi','RexhepKadriu.rks','ZenunPajazitiOfficial','mhaskukaa','shaqirtotaj','DrHatimBaxhaku','lulzimkabashi.ak','bnurboja','gazmuhaxheri','fatmirgashipeje','sabihashalaofficial','ShaipSurdulliKamenice'

//'QendronKastrati10','SmajlLatifi50','voto47','Dr.IdrizVehapi','AgimVeliu','AjetPotera2017','BekimHamezJashari','pdknaimismajli','FatmirRashitiVoto54'

//'selimshabanldk47','strpce.ldp.9','aaksuharekeofficial','asllanajsali','BlerimiperSuhareke','sokolihaliti.official','salihsalihu19','pdkdegaviti','feritidrizii'

//'xhafertahiriofficial','pdkmalisheva','VVMalisheva','ldkjunikfaqjazyrtare','KTAP.KOSOVA','kdtp.rks'

//'SrdjanPopovicZaGradonacelnika','oliverivanovickos'

///'mal.lokaj1','zekesinanajdecan','BlerandKadrijajofficial','ramizlladrovci','xhemajlajisa'

//'ProfDrBeqirSadikaj','Agron.Avdijaj10','BesimIlaziPDK','LvvKacanik','ShkelqimMJakupi','HajrushKurtajOfficial'

//'BashotaSokol','Zenunelezaj1963','besimhotikline','LVVKline','BurimBerishaOfficial','gradonacelnikjablanovic'

//'imri.ahmeti','IsmetHajrullahuPDK','SelatinRetkoceriVV','bajrushymeri','XhaferGashiPerObiliq','vetevendosjekastriot'

$kandidatetPerKryetar = ['vllahiuarber']; 
foreach ($kandidatetPerKryetar as $kandidati)
{
  Prova($kandidati,$fb,$token,$dbConn);
}

//----------------------------------------------------------------------------------------------------------------------
   /* $sqlQuery = "insert into postimetekryetarevekomunave(postiid,EmriKandidatit,EmriPartise,kohapostimit,nrLike,nrReaction,nrKomenteve,nrShare) values('','','','','','','','')";
    $dbResponse = mysqli_query($dbConn , $sqlQuery);
    if (!$dbResponse)
    {
        die('Database couldnt be reached: ' . $dbResponse );
    }
    else
    {
		echo "Sukses";
    }
	*/

?>