<?php
/**
 * Created by PhpStorm.
 * User: GentR
 * Date: 28-Mar-17
 * Time: 10:23 PM
 */

# insertData.php

require('config.php');
# fb-initialization
require_once __DIR__ . '/../facebook-sdk-v5/autoload.php';
if(!session_id()) {
    session_start();
}

$fb = new Facebook\Facebook([
    'app_id' => '1361304643927217',
    'app_secret' => '3ad8e25a982aff16c1ff87b1a1da2f5f',
    'default_graph_version' => 'v2.9',
]);

// Getting long-lived token.
$token = $longtoken;
$fb->setDefaultAccessToken($token);

// Removing time limit because reponses take longer than 30 seconds, which is default.
set_time_limit(0);

# db_Connection-initialization
$config = parse_ini_file('../secure/db_config.ini');
$dbConn = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
if (!$dbConn)
{
    die('Connection to database couldnt be established: '.mysqli_connect_error());
}
else
{
    echo 'Connected to '.$config['host'].' succesfully!';
}

function GetPageFanCount($pageName, $fb, $token, $dbConn)
{
    try {
        // Returns a `Facebook\FacebookResponse` object
        $response = $fb->get($pageName.'/?fields=fan_count,id,name', $token);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }
    echo "Response: " . $response->getBody() . "</p>\n\n";
    $jsonData = json_decode($response->getBody());
    $id = $jsonData->id;
    $name = $jsonData->name;
    $fan_count = $jsonData->fan_count;
    $date = date("Y-m-d H:i:s");
    $sqlQuery = "INSERT INTO partite (id, name, fan_count, dateInsert) VALUES ('$id', '$name', '$fan_count', '$date')";
    $dbResponse = mysqli_query($dbConn , $sqlQuery);
    // echo $sqlQuery;
    if (!$dbResponse)
    {
        die('Database couldnt be reached: ' . $dbResponse);
    }
    else
    {
       // echo 'Inserted data into db!';
    }
}


//albini2017/feed?fields=id,reactions.limit(1).summary(total_count),likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares

function Prova($pageName, $fb, $token, $dbConn)
{
    try {
        $dayseconds=strtotime("-30 Days");
        $day= date("d M Y", $dayseconds);
		//$query=$pageName."/feed?fields=id,likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares&until=now&since=$day";
		//$query=$pageName."/feed?fields=id,likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares&until=6 July 2014&since=6 May 2014";
       //	$query=$pageName."/feed?fields=id,reactions.limit(1).summary(total_count),likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares&until=now&since=$day";
	   $query=$pageName."/?fields=posts.since($day).until(now){id,reactions.limit(1).summary(total_count),likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares}";

	   
	   
	   $response = $fb->get($query, $token);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }
    $feedEdge = $response->getBody();
    $jsonData = json_decode($response->getBody());
    $data = $jsonData->posts->data;
    foreach($data as $nrPosteve)
    {
        $KohaPostimit1=$nrPosteve->created_time;
        $KohaPostimit = date("Y-m-d H:i:s", strtotime($KohaPostimit1));
        $IdPostimit=$nrPosteve->id;
        $NrKomenteve=$nrPosteve->comments->summary->total_count;
        $NrLikeve=$nrPosteve->likes->summary->total_count;
		$NrShares="";
		$nrReactions=$nrPosteve->reactions->summary->total_count;
		//echo $pageName." ".$nrReactions."<br>";
		
		if($pageName == 'nismaperkosoven.official')
		{			
			$koalicioni='PDK-AAK-NISMA';
		}
		else if ($pageName=='Aleanca.Official')
		{
			$koalicioni='PDK-AAK-NISMA';

		}
		else if ($pageName=='PDKqender')
		{
			$koalicioni='PDK-AAK-NISMA';
		}
		else if ($pageName=='drejtesiaofficial')
		{
			$koalicioni='PDK-AAK-NISMA';
		}
		else if ($pageName=='LidhjaDemokratikeeKosovesOFFICIAL')
		{
			$koalicioni='LDK-AKR-ALT';
		}
		else if ($pageName=='aleancakosovaere')
		{
			$koalicioni='LDK-AKR-ALT';
		}
		else if ($pageName=='AlternativaKosova')
		{
			$koalicioni='LDK-AKR-ALT';
		}
		else
		{
			$koalicioni='VV';
		}
		
        if(!isset($nrPosteve->shares->count))
        {
            $NrShares="0";
        }
        else
        {
            $NrShares=$nrPosteve->shares->count;
        }
        $sqlQuery = "INSERT INTO postimetepartive (postiid, partianame, kohapostimit, nrkomenteve, nrlike, nrReaction, nrshare,koalicionet) VALUES ('$IdPostimit', '$pageName', '$KohaPostimit', '$NrKomenteve','$NrLikeve','$nrReactions','$NrShares','$koalicioni')
		ON DUPLICATE KEY UPDATE
		partianame='$pageName',kohapostimit='$KohaPostimit',nrkomenteve='$NrKomenteve',nrlike='$NrLikeve',nrReaction='$nrReactions',nrshare='$NrShares',koalicionet='$koalicioni'";
        $dbResponse = mysqli_query($dbConn , $sqlQuery);
    }
    
	
    
    if(!isset($jsonData->posts->paging->next))
    {
        $i=false;
    }
    else
    {
        $i=true;
		$nextpage=$jsonData->posts->paging->next;
    }
    while($i==true)
    {
        // Nese ndron verzioni ndro explode
        $nextquery=explode('https://graph.facebook.com/v2.9',$nextpage);
        $query2Page=$nextquery[1];
        $response = $fb->get($query2Page, $token);
        $feedEdge = $response->getBody();
        $jsonData = json_decode($response->getBody());
        $data = $jsonData->data;
        foreach($data as $nrPosteve)
        {
            $KohaPostimit1=$nrPosteve->created_time;
            $KohaPostimit = date("Y-m-d H:i:s", strtotime($KohaPostimit1));
            $IdPostimit=$nrPosteve->id;
            $NrKomenteve=$nrPosteve->comments->summary->total_count;
            $NrLikeve=$nrPosteve->likes->summary->total_count;
			$nrReactions=$nrPosteve->reactions->summary->total_count;
			$NrShares="";
            if(!isset($nrPosteve->shares->count))
            {
                $NrShares="0";
            }
            else
            {
                $NrShares=$nrPosteve->shares->count;
            }
            $sqlQuery = "INSERT INTO postimetepartive (postiid, partianame, kohapostimit, nrkomenteve, nrlike, nrReaction, nrshare,koalicionet) VALUES ('$IdPostimit', '$pageName', '$KohaPostimit', '$NrKomenteve','$NrLikeve','$nrReactions','$NrShares','$koalicioni')
			ON DUPLICATE KEY UPDATE
			partianame='$pageName',kohapostimit='$KohaPostimit',nrkomenteve='$NrKomenteve',nrlike='$NrLikeve',nrReaction='$nrReactions',nrshare='$NrShares',koalicionet='$koalicioni'";
            $dbResponse = mysqli_query($dbConn , $sqlQuery);
        }
         if(!isset($jsonData->paging->next))
        {
            $i=false;
        }
		else
		{
			$nextpage=$jsonData->paging->next;
       
		}
    }
}

function MerrPostetEKandidateve($pageName, $fb, $token, $dbConn)
{
    try {
        // Returns a `Facebook\FacebookResponse` object
        $dayseconds=strtotime("-30 Days");
        $day= date("d M Y", $dayseconds);
      //  $query=$pageName."/feed?fields=id,likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares&until=now&since=$day";
	//	$query=$pageName."/feed?fields=id,reactions.limit(1).summary(total_count),likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares&until=now&since=$day";
	$query=$pageName."/?fields=posts.since($day).until(now){id,reactions.limit(1).summary(total_count),likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares}";
	
        $response = $fb->get($query, $token);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }
    $feedEdge = $response->getBody();
	//var_dump($feedEdge);
    $jsonData = json_decode($response->getBody());
    $data = $jsonData->posts->data;
    foreach($data as $nrPosteve)
    {
        $KohaPostimit1=$nrPosteve->created_time;
        $KohaPostimit = date("Y-m-d H:i:s", strtotime($KohaPostimit1));
        $IdPostimit=$nrPosteve->id;
        $NrKomenteve=$nrPosteve->comments->summary->total_count;
        $NrLikeve=$nrPosteve->likes->summary->total_count;
		$nrReactions=$nrPosteve->reactions->summary->total_count;
        if(!isset($nrPosteve->shares->count))
        {
            $NrShares="0";
        }
        else
        {
            $NrShares=$nrPosteve->shares->count;
        }
        $sqlQuery = "INSERT INTO postimetekandidateve (postiid, kandidatiname, kohapostimit, nrkomenteve, nrlike, nrReaction, nrshare) VALUES ('$IdPostimit', '$pageName', '$KohaPostimit', '$NrKomenteve','$NrLikeve','$nrReactions','$NrShares')
		ON DUPLICATE KEY UPDATE
		kandidatiname='$pageName',kohapostimit='$KohaPostimit',nrkomenteve='$NrKomenteve',nrlike='$NrLikeve',nrReaction='$nrReactions',nrshare='$NrShares'";
        $dbResponse = mysqli_query($dbConn , $sqlQuery);
        //echo $sqlQuery."<br><br>";
    }
	
    if(!isset($jsonData->posts->paging->next))
    {
        $i=false;
    }
    else
    {
        $i=true;
		$nextpage=$jsonData->posts->paging->next;

    }
    while($i==true)
    {
		
		echo "<br><br><br><br><br><br><br><br>";
        // Nese ndron verzioni ndro explode
        $nextquery=explode('https://graph.facebook.com/v2.9',$nextpage);
        $query2Page=$nextquery[1];
        $response = $fb->get($query2Page, $token);
        $feedEdge = $response->getBody();
        $jsonData = json_decode($response->getBody());
        $data = $jsonData->data;
        foreach($data as $nrPosteve)
        {
            $KohaPostimit1=$nrPosteve->created_time;
            $KohaPostimit = date("Y-m-d H:i:s", strtotime($KohaPostimit1));
            $IdPostimit=$nrPosteve->id;
            $NrKomenteve=$nrPosteve->comments->summary->total_count;
            $NrLikeve=$nrPosteve->likes->summary->total_count;
			$nrReactions=$nrPosteve->reactions->summary->total_count;
            if(!isset($nrPosteve->shares->count))
            {
                $NrShares="0";
            }
            else
            {
                $NrShares=$nrPosteve->shares->count;
            }
            $sqlQuery = "INSERT INTO postimetekandidateve (postiid, kandidatiname, kohapostimit, nrkomenteve, nrlike, nrReaction, nrshare) VALUES ('$IdPostimit', '$pageName', '$KohaPostimit', '$NrKomenteve','$NrLikeve','$nrReactions','$NrShares')
			ON DUPLICATE KEY UPDATE
			kandidatiname='$pageName',kohapostimit='$KohaPostimit',nrkomenteve='$NrKomenteve',nrlike='$NrLikeve',nrReaction='$nrReactions',nrshare='$NrShares'";
            $dbResponse = mysqli_query($dbConn , $sqlQuery);
        }

        if(!isset($jsonData->paging->next))
        {
            $i=false;
        }
		else
		{
			$nextpage=$jsonData->paging->next;
       
		}
    }
}


function MerrPostetEFiguravePolitike($pageName, $fb, $token, $dbConn)
{
    try {
        // Returns a `Facebook\FacebookResponse` object
        $dayseconds=strtotime("-30 Days");
        $day= date("d M Y", $dayseconds);
        //  $query=$pageName."/feed?fields=id,likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares&until=now&since=$day";
        //	$query=$pageName."/feed?fields=id,reactions.limit(1).summary(total_count),likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares&until=now&since=$day";
        $query=$pageName."/?fields=posts.since($day).until(now){id,reactions.limit(1).summary(total_count),likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares}";

        $response = $fb->get($query, $token);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }
    $feedEdge = $response->getBody();
    //var_dump($feedEdge);
    $jsonData = json_decode($response->getBody());
    $data = $jsonData->posts->data;
    foreach($data as $nrPosteve)
    {
        $KohaPostimit1=$nrPosteve->created_time;
        $KohaPostimit = date("Y-m-d H:i:s", strtotime($KohaPostimit1));
        $IdPostimit=$nrPosteve->id;
        $NrKomenteve=$nrPosteve->comments->summary->total_count;
        $NrLikeve=$nrPosteve->likes->summary->total_count;
        $nrReactions=$nrPosteve->reactions->summary->total_count;
        if(!isset($nrPosteve->shares->count))
        {
            $NrShares="0";
        }
        else
        {
            $NrShares=$nrPosteve->shares->count;
        }
        $sqlQuery = "INSERT INTO postimetefiguravepolitike (postiid, kandidatiname, kohapostimit, nrkomenteve, nrlike, nrReaction, nrshare) VALUES ('$IdPostimit', '$pageName', '$KohaPostimit', '$NrKomenteve','$NrLikeve','$nrReactions','$NrShares')
		ON DUPLICATE KEY UPDATE
		kandidatiname='$pageName',kohapostimit='$KohaPostimit',nrkomenteve='$NrKomenteve',nrlike='$NrLikeve',nrReaction='$nrReactions',nrshare='$NrShares'";
        $dbResponse = mysqli_query($dbConn , $sqlQuery);
        //echo $sqlQuery."<br><br>";
    }

    if(!isset($jsonData->posts->paging->next))
    {
        $i=false;
    }
    else
    {
        $i=true;
        $nextpage=$jsonData->posts->paging->next;

    }
    while($i==true)
    {

        echo "<br><br><br><br><br><br><br><br>";
        // Nese ndron verzioni ndro explode
        $nextquery=explode('https://graph.facebook.com/v2.9',$nextpage);
        $query2Page=$nextquery[1];
        $response = $fb->get($query2Page, $token);
        $feedEdge = $response->getBody();
        $jsonData = json_decode($response->getBody());
        $data = $jsonData->data;
        foreach($data as $nrPosteve)
        {
            $KohaPostimit1=$nrPosteve->created_time;
            $KohaPostimit = date("Y-m-d H:i:s", strtotime($KohaPostimit1));
            $IdPostimit=$nrPosteve->id;
            $NrKomenteve=$nrPosteve->comments->summary->total_count;
            $NrLikeve=$nrPosteve->likes->summary->total_count;
            $nrReactions=$nrPosteve->reactions->summary->total_count;
            if(!isset($nrPosteve->shares->count))
            {
                $NrShares="0";
            }
            else
            {
                $NrShares=$nrPosteve->shares->count;
            }
            $sqlQuery = "INSERT INTO postimetefiguravepolitike (postiid, kandidatiname, kohapostimit, nrkomenteve, nrlike, nrReaction, nrshare) VALUES ('$IdPostimit', '$pageName', '$KohaPostimit', '$NrKomenteve','$NrLikeve','$nrReactions','$NrShares')
			ON DUPLICATE KEY UPDATE
			kandidatiname='$pageName',kohapostimit='$KohaPostimit',nrkomenteve='$NrKomenteve',nrlike='$NrLikeve',nrReaction='$nrReactions',nrshare='$NrShares'";
            $dbResponse = mysqli_query($dbConn , $sqlQuery);
        }

        if(!isset($jsonData->paging->next))
        {
            $i=false;
        }
        else
        {
            $nextpage=$jsonData->paging->next;

        }
    }
}



$partite = ['vetevendosje','nismaperkosoven.official','Aleanca.Official','LidhjaDemokratikeeKosovesOFFICIAL',
    'PDKqender','aleancakosovaere','drejtesiaofficial','AlternativaKosova'];
foreach ($partite as $partia)
{
 GetPageFanCount($partia,$fb,$token,$dbConn);
  Prova($partia,$fb,$token,$dbConn);
}
// kadriveseliofficial,fatmirlimaj,behgjeti,IsaMustafaKS,mimozakusari,HashimThaciOfficial
$kryeministrat =['RamushHaradinajOfficial','albini2017','avdullah.hoti'];
foreach ($kryeministrat as $kryeminitri)
{
    MerrPostetEKandidateve($kryeminitri,$fb,$token,$dbConn);
}

$figuratpolitike=['RamushHaradinajOfficial','HashimThaciOfficial','albini2017','avdullah.hoti','kadriveseliofficial','fatmirlimaj','behgjeti','IsaMustafaKS','mimozakusari','DardanMolliqaj8'];

foreach ($figuratpolitike as $figp)
{
   // MerrPostetEFiguravePolitike($figp,$fb,$token,$dbConn);
}
MerrPostetEFiguravePolitike('DardanMolliqaj8',$fb,$token,$dbConn);
//Prova('vetevendosje',$fb,$token,$dbConn);
# vetevendosje/?fields=fan_count,id,name,posts{message,likes.limit(1).summary(true),comments.limit(1).summary(true),shares,created_time}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>insertData</title>
</head>
<body>
</body>
</html>