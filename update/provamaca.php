<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 6/1/2017
 * Time: 15:26
 */



# db_Connection-initialization
$config = parse_ini_file('../secure/db_config.ini');
$dbConn = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);

function Prova($pageName, $fb, $token, $dbConn)
{
	header("Content-Type: application/json");



require('config.php');
# fb-initialization
require_once __DIR__ . '/../facebook-sdk-v5/autoload.php';
if(!session_id()) {
    session_start();
}

$fb = new Facebook\Facebook([
    'app_id' => '1361304643927217',
    'app_secret' => '3ad8e25a982aff16c1ff87b1a1da2f5f',
    'default_graph_version' => 'v2.10',
]);

// Getting long-lived token.
$token = $longtoken;
$fb->setDefaultAccessToken($token);

// Removing time limit because reponses take longer than 30 seconds, which is default.
set_time_limit(0);


if (!$dbConn)
{
    die('Connection to database couldnt be established: '.mysqli_connect_error());
}
else
{
    echo 'Connected to '.$config['host'].' succesfully!';
}
    try {
        $dayseconds=strtotime("-10 Days");
        $day= date("d M Y", $dayseconds);
		//$query=$pageName."/feed?fields=id,likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares&until=now&since=$day";
		//$query=$pageName."/feed?fields=id,likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares&until=6 July 2014&since=6 May 2014";
       //	$query=$pageName."/feed?fields=id,reactions.limit(1).summary(total_count),likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares&until=now&since=$day";
	   $query=$pageName."/?fields=posts.since($day).until(now){id,reactions.limit(1).summary(total_count),likes.limit(1).summary(true),created_time,comments.limit(1).summary(true),shares}";

	   
	   
	   $response = $fb->get($query, $token);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }
    $feedEdge = $response->getBody();
    $jsonData = json_decode($response->getBody());
    $data = $jsonData->posts->data;
    foreach($data as $nrPosteve)
    {
        $KohaPostimit1=$nrPosteve->created_time;
        $KohaPostimit = date("Y-m-d H:i:s", strtotime($KohaPostimit1));
        $IdPostimit=$nrPosteve->id;
        $NrKomenteve=$nrPosteve->comments->summary->total_count;
        $NrLikeve=$nrPosteve->likes->summary->total_count;
		$NrShares="";
		$nrReactions=$nrPosteve->reactions->summary->total_count;
		//echo $pageName." ".$nrReactions." ";
		
		if($pageName == 'ShpendAhmetii' || $pageName == 'CaushiDriton' || $pageName == 'KryetarFehmiFerati' || $pageName == 'FatonTopalli2017' || $pageName == 'SamiKurteshi13' || $pageName == 'mhaskukaa' || $pageName == 'bnurboja' || $pageName=='QendronKastrati10' || $pageName=='AjetPotera2017' || $pageName=='Besim-Muzaqi-310775596061589' || $pageName=='VVMalisheva')
		{			
			$partia='Lëvizja VETËVENDOSJE!';
		}
		else if ($pageName=='abrashiarban' || $pageName == 'safetkamberipermitrovicen' || $pageName == 'muharremsvarqazyrtare' || $pageName == 'hazirilutfi' || $pageName == 'DrHatimBaxhaku' || $pageName == 'gazmuhaxheri' || $pageName=='ShaipSurdulliKamenice'|| $pageName=='voto47'|| $pageName=='selimshabanldk47'|| $pageName=='asllanajsali'|| $pageName=='sokolihaliti.official'|| $pageName=='xhafertahiriofficial'|| $pageName=='ldkjunikfaqjazyrtare')
		{
			$partia='LDK';

		}
		else if ($pageName=='selimpacolliofficial' || $pageName == 'Agim.Bahtirii' || $pageName=='Musa-DERVISHAJ-1471112073125298')
		{
			$partia='AKR';
		}
		else if ($pageName=='LirakCelaj.ks' || $pageName == 'RamadanHotii' || $pageName == 'validrizi' || $pageName == 'AgimAliuPDK94' || $pageName == 'ZenunPajazitiOfficial' || $pageName == 'shaqirtotaj' || $pageName =='sabihashalaofficial' || $pageName=='Dr.IdrizVehapi' || $pageName=='Dr.IdrizVehapi' || $pageName=='Hajredin-Hyseni-Dini-491857897815166' || $pageName=='pdknaimismajli' || $pageName=='BlerimiperSuhareke' || $pageName=='pdkdegaviti' || $pageName=='feritidrizii' || $pageName=='pdkmalisheva' )
		{
			$partia='PDK';
		}
		else if ($pageName=='vllahiuarber' || $pageName == 'ardiangjiniaak' || $pageName == 'XhavitZariqiOfficial' || $pageName == 'RexhepKadriu.rks' || $pageName == 'lulzimkabashi.ak' || $pageName == 'fatmirgashipeje' || $pageName == 'SmajlLatifi50' || $pageName=='aaksuharekeofficial' || $pageName=='Agron-Kuçi-222542211233814')
		{
			$partia='AAK';
		}
		else if($pageName=='mimozakusari')
		{
		$partia="ALT";
		}
		else if($pageName=='BekimHamezJashari' || $pageName=='FatmirRashitiVoto54')
		{
			$partia="KP";
		}
		else if ($pageName=='strpce.ldp.9' || $pageName=='SrdjanPopovicZaGradonacelnika' || $pageName=='Srpska-lista-Parteš-Pasjane-1914605832085405' || $pageName=='Goran-Rakic-Srpska-Lista-130664660988134')
		{
			$partia="SL";
		}
		else if($pageName=='oliverivanovickos')
		{
			$partia='IQ';
		}
		else if($pageName=='KTAP.KOSOVA')
		{
			$partia='KTAP';
		}
		else if($pageName=='kdtp.rks')
		{
			$partia='KDTP';
		}
		
        if(!isset($nrPosteve->shares->count))
        {
            $NrShares="0";
        }
        else
        {
            $NrShares=$nrPosteve->shares->count;
        }
        $sqlQuery = "insert into postimetekryetarevekomunave(postiid,EmriKandidatit,EmriPartise,kohapostimit,nrLike,nrReaction,nrKomenteve,nrShare) VALUES ('$IdPostimit', '$pageName','$partia', '$KohaPostimit','$NrLikeve','$nrReactions','$NrKomenteve','$NrShares')
		ON DUPLICATE KEY UPDATE
		EmriKandidatit='$pageName',EmriPartise='$partia',kohapostimit='$KohaPostimit',nrlike='$NrLikeve',nrReaction='$nrReactions',nrkomenteve='$NrKomenteve',nrshare='$NrShares'";
        $dbResponse = mysqli_query($dbConn , $sqlQuery);

    }
    
	
    
    if(!isset($jsonData->posts->paging->next))
    {
        $i=false;
    }
    else
    {
        $i=true;
		$nextpage=$jsonData->posts->paging->next;
    }
    while($i==true)
    {
        // Nese ndron verzioni ndro explode
        $nextquery=explode('https://graph.facebook.com/v2.10',$nextpage);
        $query2Page=$nextquery[1];
        $response = $fb->get($query2Page, $token);
        $feedEdge = $response->getBody();
        $jsonData = json_decode($response->getBody());
        $data = $jsonData->data;
        foreach($data as $nrPosteve)
        {
            $KohaPostimit1=$nrPosteve->created_time;
            $KohaPostimit = date("Y-m-d H:i:s", strtotime($KohaPostimit1));
            $IdPostimit=$nrPosteve->id;
            $NrKomenteve=$nrPosteve->comments->summary->total_count;
            $NrLikeve=$nrPosteve->likes->summary->total_count;
			$nrReactions=$nrPosteve->reactions->summary->total_count;
			$NrShares="";
            if(!isset($nrPosteve->shares->count))
            {
                $NrShares="0";
            }
            else
            {
                $NrShares=$nrPosteve->shares->count;
            }
            $sqlQuery = "insert into postimetekryetarevekomunave(postiid,EmriKandidatit,EmriPartise,kohapostimit,nrLike,nrReaction,nrKomenteve,nrShare) VALUES ('$IdPostimit', '$pageName','$partia', '$KohaPostimit','$NrLikeve','$nrReactions','$NrKomenteve','$NrShares')
		ON DUPLICATE KEY UPDATE
		EmriKandidatit='$pageName',EmriPartise='$partia',kohapostimit='$KohaPostimit',nrlike='$NrLikeve',nrReaction='$nrReactions',nrkomenteve='$NrKomenteve',nrshare='$NrShares'";
            $dbResponse = mysqli_query($dbConn , $sqlQuery);
			
        }
         if(!isset($jsonData->paging->next))
        {
            $i=false;
        }
		else
		{
			$nextpage=$jsonData->paging->next;
       
		}
    }
}


$kandidatetPerKryetar = ['ShpendAhmetii','abrashiarban','selimpacolliofficial','LirakCelaj.ks','vllahiuarber','CaushiDriton','ardiangjiniaak','mimozakusari','RamadanHotii','KryetarFehmiFerati','Agim.Bahtirii','validrizi','safetkamberipermitrovicen','FatonTopalli2017','muharremsvarqazyrtare','XhavitZariqiOfficial','AgimAliuPDK94','SamiKurteshi13','hazirilutfi','RexhepKadriu.rks','ZenunPajazitiOfficial','mhaskukaa','shaqirtotaj','DrHatimBaxhaku','lulzimkabashi.ak','bnurboja','gazmuhaxheri','fatmirgashipeje','sabihashalaofficial','ShaipSurdulliKamenice','QendronKastrati10','SmajlLatifi50','voto47','Dr.IdrizVehapi','AgimVeliu','AjetPotera2017','Hajredin-Hyseni-Dini-491857897815166','BekimHamezJashari','Musa-DERVISHAJ-1471112073125298','pdknaimismajli','FatmirRashitiVoto54','selimshabanldk47','strpce.ldp.9','aaksuharekeofficial','asllanajsali','BlerimiperSuhareke','sokolihaliti.official','salihsalihu19','pdkdegaviti','feritidrizii','xhafertahiriofficial','Besim-Muzaqi-310775596061589','RAGIP-BEGAJ-533546846720711','pdkmalisheva','VVMalisheva','Agron-Kuçi-222542211233814','ldkjunikfaqjazyrtare','KTAP.KOSOVA','kdtp.rks','SrdjanPopovicZaGradonacelnika','Srpska-lista-Parteš-Pasjane-1914605832085405','Goran-Rakic-Srpska-Lista-130664660988134','oliverivanovickos'];
foreach ($kandidatetPerKryetar as $kandidati)
{
  Prova($kandidati,"","",$dbConn);//,$fb,$token,$dbConn);
}

//----------------------------------------------------------------------------------------------------------------------
   /* $sqlQuery = "insert into postimetekryetarevekomunave(postiid,EmriKandidatit,EmriPartise,kohapostimit,nrLike,nrReaction,nrKomenteve,nrShare) values('','','','','','','','')";
    $dbResponse = mysqli_query($dbConn , $sqlQuery);
    if (!$dbResponse)
    {
        die('Database couldnt be reached: ' . $dbResponse );
    }
    else
    {
		echo "Sukses";
    }
	*/

?>