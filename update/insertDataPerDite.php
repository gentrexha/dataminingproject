<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 6/2/2017
 * Time: 13:22
 */
set_time_limit(0);

$config = parse_ini_file('../secure/db_config.ini');
$dbConn = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
if (!$dbConn)
{
    die('Connection to database couldnt be established: '.mysqli_connect_error());
}

$sqlQuery = "SELECT DATE_FORMAT(m1, '%Y-%m-%d') data FROM ( SELECT SUBDATE( NOW() , INTERVAL 31 DAY) + INTERVAL m DAY AS m1 FROM ( select @rownum:=@rownum+1 as m from (select 1 union select 2 union select 3 union select 4) t1, (select 1 union select 2 union select 3 union select 4) t2, (select 1 union select 2 union select 3 union select 4) t3, (select 1 union select 2 union select 3 union select 4) t4, (select @rownum:=-1) t0 ) d1 ) d2 WHERE m1 <= now()- INTERVAL 1 DAY ORDER BY m1 desc";
$dbResponse = mysqli_query($dbConn , $sqlQuery);

while($row = mysqli_fetch_assoc($dbResponse)) {
    $muajiFundit[] =$row["data"];
}


foreach($muajiFundit as $dita)
{

    // Insert i te dhenave per figurat politike
    $sqlQuery1 = "select kandidatiname,date(kohapostimit) dataa ,sum(nrkomenteve) komentee,sum(nrlike) likee ,sum(nrshare) sharee,sum(nrReaction) reaction,count(*) totpost from postimetefiguravepolitike
WHERE   date(kohapostimit) ='$dita'
group by kandidatiname,dataa
UNION
select DISTINCT kandidatiname,'$dita' as dataa,'0' as komentee,'0' as likee,'0' as sharee,'0' as reaction,'0' as totpost
FROM   postimetefiguravepolitike
WHERE kandidatiname NOT IN (
select kandidatiname from postimetefiguravepolitike WHERE   date(kohapostimit) = '$dita' group by kandidatiname)";
    $dbResponse1 = mysqli_query($dbConn , $sqlQuery1);

    while($row1 = mysqli_fetch_assoc($dbResponse1)) {
        $emri=$row1["kandidatiname"];
        $data=$row1["dataa"];
        $nrLike=$row1["likee"];
        $nrReaction=$row1["reaction"];
        $nrKomente=$row1["komentee"];
        $nrShare=$row1["sharee"];
        $nrPosteve=$row1["totpost"];

        $sql = "INSERT INTO datarrethfiguravepolitike (EmriKandidatit, Data, nrLike,nrReaction,nrKomente,nrShare,nrPosteve) VALUES ('$emri', '$data', '$nrLike', '$nrReaction', '$nrKomente', '$nrShare', '$nrPosteve')ON DUPLICATE KEY UPDATE
			Data='$data',nrLike='$nrLike',nrReaction='$nrReaction',nrKomente='$nrKomente',nrShare='$nrShare',nrPosteve='$nrPosteve'";

        mysqli_query($dbConn, $sql);

    }

    //Insert i te dhenave per partite

    $sqlQuery1 = "select partianame,date(kohapostimit) dataa ,sum(nrkomenteve) komentee,sum(nrlike) likee ,sum(nrshare) sharee,sum(nrReaction) reaction,count(*) totpost from postimetepartive
WHERE   date(kohapostimit) ='$dita'
group by partianame,dataa
UNION
select DISTINCT partianame,'$dita' as dataa,'0' as komentee,'0' as likee,'0' as sharee,'0' as reaction,'0' as totpost
FROM   postimetepartive
WHERE partianame NOT IN (
select partianame from postimetepartive WHERE   date(kohapostimit) = '$dita' group by partianame)";
    $dbResponse1 = mysqli_query($dbConn , $sqlQuery1);

    while($row1 = mysqli_fetch_assoc($dbResponse1)) {
        $emri=$row1["partianame"];
        $data=$row1["dataa"];
        $nrLike=$row1["likee"];
        $nrReaction=$row1["reaction"];
        $nrKomente=$row1["komentee"];
        $nrShare=$row1["sharee"];
        $nrPosteve=$row1["totpost"];
        $sql = "INSERT INTO datarrethpartive (EmriPartise, Data, nrLike,nrReaction,nrKomente,nrShare,nrPosteve) VALUES ('$emri', '$data', '$nrLike', '$nrReaction', '$nrKomente', '$nrShare', '$nrPosteve')ON DUPLICATE KEY UPDATE
			Data='$data',nrLike='$nrLike',nrReaction='$nrReaction',nrKomente='$nrKomente',nrShare='$nrShare',nrPosteve='$nrPosteve'";

        mysqli_query($dbConn, $sql);



    }

//Insert i te dhenave per koalicioniet

    $sqlQuery1 = "select koalicionet,date(kohapostimit) dataa ,sum(nrkomenteve) komentee,sum(nrlike) likee ,sum(nrshare) sharee,sum(nrReaction) reaction,count(*) totpost from postimetepartive
WHERE   date(kohapostimit) ='$dita'
group by koalicionet,dataa
UNION
select DISTINCT koalicionet,'$dita' as dataa,'0' as komentee,'0' as likee,'0' as sharee,'0' as reaction,'0' as totpost
FROM   postimetepartive
WHERE koalicionet NOT IN (
select koalicionet from postimetepartive WHERE   date(kohapostimit) = '$dita' group by koalicionet)";
    $dbResponse1 = mysqli_query($dbConn , $sqlQuery1);

    while($row1 = mysqli_fetch_assoc($dbResponse1)) {
        $emri=$row1["koalicionet"];
        $data=$row1["dataa"];
        $nrLike=$row1["likee"];
        $nrReaction=$row1["reaction"];
        $nrKomente=$row1["komentee"];
        $nrShare=$row1["sharee"];
        $nrPosteve=$row1["totpost"];
        $sql = "INSERT INTO datarrethkoalicioneve (EmriKoalicionit, Data, nrLike,nrReaction,nrKomente,nrShare,nrPosteve) VALUES ('$emri', '$data', '$nrLike', '$nrReaction', '$nrKomente', '$nrShare', '$nrPosteve')ON DUPLICATE KEY UPDATE
			Data='$data',nrLike='$nrLike',nrReaction='$nrReaction',nrKomente='$nrKomente',nrShare='$nrShare',nrPosteve='$nrPosteve'";

        mysqli_query($dbConn, $sql);



    }

//Inser i te dhenave per partite ne zgjedhjet e 2014

    $sqlQuery1 = "select partianame,date(kohapostimit) dataa ,sum(nrkomenteve) komentee,sum(nrlike) likee ,sum(nrshare) sharee,sum(nrReaction) reaction,count(*) totpost from postimetepartive
WHERE   date(kohapostimit) ='$dita'
group by partianame,dataa
UNION
select DISTINCT partianame,'$dita' as dataa,'0' as komentee,'0' as likee,'0' as sharee,'0' as reaction,'0' as totpost
FROM   postimetepartive
WHERE partianame NOT IN (
select partianame from postimetepartive WHERE   date(kohapostimit) = '$dita' group by partianame)";
    $dbResponse1 = mysqli_query($dbConn , $sqlQuery1);

    while($row1 = mysqli_fetch_assoc($dbResponse1)) {
        $emri = $row1["partianame"];
        $data = $row1["dataa"];
        $nrLike = $row1["likee"];
        $nrReaction = $row1["reaction"];
        $nrKomente = $row1["komentee"];
        $nrShare = $row1["sharee"];
        $nrPosteve = $row1["totpost"];

        $sql = "INSERT INTO datarrethpartive (EmriPartise, Data, nrLike,nrReaction,nrKomente,nrShare,nrPosteve) VALUES ('$emri', '$data', '$nrLike', '$nrReaction', '$nrKomente', '$nrShare', '$nrPosteve')ON DUPLICATE KEY UPDATE
			Data='$data',nrLike='$nrLike',nrReaction='$nrReaction',nrKomente='$nrKomente',nrShare='$nrShare',nrPosteve='$nrPosteve'";

        mysqli_query($dbConn, $sql);
    }


//Insert i te dhenave per kandidatet per kryeminister

    	$sqlQuery1 = "select kandidatiname,date(kohapostimit) dataa ,sum(nrkomenteve) komentee,sum(nrlike) likee ,sum(nrshare) sharee,sum(nrReaction) reaction,count(*) totpost from postimetekandidateve
WHERE   date(kohapostimit) ='$dita'
group by kandidatiname,dataa
UNION
select DISTINCT kandidatiname,'$dita' as dataa,'0' as komentee,'0' as likee,'0' as sharee,'0' as reaction,'0' as totpost
FROM   postimetekandidateve
WHERE kandidatiname NOT IN (
select kandidatiname from postimetekandidateve WHERE   date(kohapostimit) = '$dita' group by kandidatiname)";
	$dbResponse1 = mysqli_query($dbConn , $sqlQuery1);

	while($row1 = mysqli_fetch_assoc($dbResponse1)) {
        $emri = $row1["kandidatiname"];
        $data = $row1["dataa"];
        $nrLike = $row1["likee"];
        $nrReaction = $row1["reaction"];
        $nrKomente = $row1["komentee"];
        $nrShare = $row1["sharee"];
        $nrPosteve = $row1["totpost"];

        $sql = "INSERT INTO datarrethkandidateve (EmriKandidatit, Data, nrLike,nrReaction,nrKomente,nrShare,nrPosteve) VALUES ('$emri', '$data', '$nrLike', '$nrReaction', '$nrKomente', '$nrShare', '$nrPosteve')ON DUPLICATE KEY UPDATE
			Data='$data',nrLike='$nrLike',nrReaction='$nrReaction',nrKomente='$nrKomente',nrShare='$nrShare',nrPosteve='$nrPosteve'";

        mysqli_query($dbConn, $sql);

    }


    // Insert i te dhenave per universitetet
    $sqlQuery1 = "select partianame,date(kohapostimit) dataa ,sum(nrkomenteve) komentee,sum(nrlike) likee ,sum(nrshare) sharee,sum(nrReaction) reaction,count(*) totpost from postimetuniversiteteve
WHERE   date(kohapostimit) ='$dita'
group by partianame,dataa
UNION
select DISTINCT partianame,'$dita' as dataa,'0' as komentee,'0' as likee,'0' as sharee,'0' as reaction,'0' as totpost
FROM   postimetuniversiteteve
WHERE partianame NOT IN (
select partianame from postimetuniversiteteve WHERE   date(kohapostimit) = '$dita' group by partianame)";
    $dbResponse1 = mysqli_query($dbConn , $sqlQuery1);

    while($row1 = mysqli_fetch_assoc($dbResponse1)) {
        $emri=$row1["partianame"];
        $data=$row1["dataa"];
        $nrLike=$row1["likee"];
        $nrReaction=$row1["reaction"];
        $nrKomente=$row1["komentee"];
        $nrShare=$row1["sharee"];
        $nrPosteve=$row1["totpost"];

        $sql = "INSERT INTO datarrethuniversiteteve (EmriKandidatit, Data, nrLike,nrReaction,nrKomente,nrShare,nrPosteve) VALUES ('$emri', '$data', '$nrLike', '$nrReaction', '$nrKomente', '$nrShare', '$nrPosteve')ON DUPLICATE KEY UPDATE
			Data='$data',nrLike='$nrLike',nrReaction='$nrReaction',nrKomente='$nrKomente',nrShare='$nrShare',nrPosteve='$nrPosteve'";

        mysqli_query($dbConn, $sql);

    }

}