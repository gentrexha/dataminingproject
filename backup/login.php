<?php
/**
 * Created by PhpStorm.
 * User: GentR
 * Date: 28-Mar-17
 * Time: 8:21 PM
 */

# login.php
if(!session_id()) {
    session_start();
}
require_once __DIR__ . '../facebook-sdk-v5/autoload.php';

$fb = new Facebook\Facebook([
    'app_id' => '1361304643927217',
    'app_secret' => '3ad8e25a982aff16c1ff87b1a1da2f5f',
    'default_graph_version' => 'v2.8',
]);

$helper = $fb->getRedirectLoginHelper();
$permissions = ['email', 'user_likes']; // optional
$loginUrl = $helper->getLoginUrl('http://www.projektitelulja.com/DataMiningProject/login-callback.php', $permissions);

echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';