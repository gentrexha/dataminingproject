<?php
/**
 * Created by PhpStorm.
 * User: GentR
 * Date: 28-Mar-17
 * Time: 8:45 PM
 */

# login-callback.php
if(!session_id()) {
    session_start();
}
require_once __DIR__ . '../facebook-sdk-v5/autoload.php';

$fb = new Facebook\Facebook([
    'app_id' => '1361304643927217',
    'app_secret' => '3ad8e25a982aff16c1ff87b1a1da2f5f',
    'default_graph_version' => 'v2.8',
]);

$helper = $fb->getRedirectLoginHelper();
try {
    $accessToken = $helper->getAccessToken();
} catch(Facebook\Exceptions\FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}

if (isset($accessToken)) {
    // Logged in!
    $_SESSION['facebook_access_token'] = (string) $accessToken;
    echo '<a href="http://www.projektitelulja.com/DataMiningProject/insertData.php">It Worked!</a>';

    // Now you can redirect to another page and use the
    // access token from $_SESSION['facebook_access_token']
}