<?php
/**
 * Created by PhpStorm.
 * User: GentR
 * Date: 03-Apr-17
 * Time: 9:13 AM
 */

# getData.php

header("Content-Type: application/json");

# db_Connection-initialization
$config = parse_ini_file('../../secure/db_config.ini');
$dbConn = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
if (!$dbConn)
{
    die('Connection to database couldnt be established: '.mysqli_connect_error());
}

if(isset($_REQUEST["t"])) {

    $Emri = strval($_REQUEST["t"]);

    $vlerat = explode(";", $Emri);
    $Menyra = mysqli_real_escape_string($dbConn,$vlerat[0]);
    $Fillimi = mysqli_real_escape_string($dbConn,$vlerat[1]);
    $Mbarimi = mysqli_real_escape_string($dbConn,$vlerat[2]);


    $sqlQuery = "select EmriKoalicionit,data ,sum($Menyra) rate from datarrethkoalicioneve
WHERE   data BETWEEN '$Fillimi' AND '$Mbarimi'
group by EmriKoalicionit,data
order by data,EmriKoalicionit asc";

    if($Menyra=="TEGJITHA")
    $sqlQuery = "select EmriKoalicionit,data ,nrKomente*0.3196+nrLike*0.2896+nrShare*0.3908 rate from datarrethkoalicioneve
WHERE   data BETWEEN '$Fillimi' AND '$Mbarimi'
group by EmriKoalicionit,data
order by data,EmriKoalicionit asc";
    $dbResponse = mysqli_query($dbConn, $sqlQuery);
    if (!$dbResponse) {
        die('Database couldnt be reached: ' . $dbResponse);
    } else {
        $rows = array();
        while ($r = mysqli_fetch_assoc($dbResponse)) {
            $rows[] = $r;
        }
        print json_encode($rows, JSON_PRETTY_PRINT);
    }
}
?>