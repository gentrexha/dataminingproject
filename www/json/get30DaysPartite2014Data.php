<?php
/**
 * Created by PhpStorm.
 * User: GentR
 * Date: 03-Apr-17
 * Time: 9:13 AM
 */

# getData.php

header("Content-Type: application/json");

# db_Connection-initialization
$config = parse_ini_file('../../secure/db_config.ini');
$dbConn = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
if (!$dbConn)
{
    die('Connection to database couldnt be established: '.mysqli_connect_error());
}


$PartiaEmri="";
if(isset($_REQUEST["t"]))
{
	
	$Emri=strval($_REQUEST["t"]);
	$vlerat=explode(";",$Emri);
	$Menyra=mysqli_real_escape_string($dbConn,$vlerat[0]);
	$Fillimi2014=mysqli_real_escape_string($dbConn,$vlerat[1]);
	$Mbarimi2014=mysqli_real_escape_string($dbConn,$vlerat[2]);
	$Fillimi2017=mysqli_real_escape_string($dbConn,$vlerat[3]);
	$Mbarimi2017=mysqli_real_escape_string($dbConn,$vlerat[4]);
	$PartiaEmri=mysqli_real_escape_string($dbConn,$vlerat[5]);

if($Menyra=="TEGJITHA")
{
$query="create or replace view d2014 as
select EmriPartise,Data,nrKomente*0.3196+nrLike*0.2896+nrShare*0.3908 rate
from datarrethpartive where EmriPartise = '$PartiaEmri' and Data between '$Fillimi2014' and '$Mbarimi2014'
group by EmriPartise,Data 
order by Data,EmriPartise;";
$dbResponseView = mysqli_query($dbConn , $query);
$query="create or replace view d2017 as 
select EmriPartise,Data,nrKomente*0.3196+nrLike*0.2896+nrShare*0.3908 rate
from datarrethpartive where EmriPartise = '$PartiaEmri' and Data between '$Fillimi2017' and '$Mbarimi2017' 
group by EmriPartise,Data 
order by Data,EmriPartise;";
$dbResponseView = mysqli_query($dbConn , $query);
}
else
{
    $query="create or replace view d2014 as
select EmriPartise,Data,sum($Menyra) rate
from datarrethpartive where EmriPartise = '$PartiaEmri' and Data between '$Fillimi2014' and '$Mbarimi2014'
group by EmriPartise,Data 
order by Data,EmriPartise;";
    $dbResponseView = mysqli_query($dbConn , $query);
    $query="create or replace view d2017 as 
select EmriPartise,Data,sum($Menyra) rate
from datarrethpartive where EmriPartise = '$PartiaEmri' and Data between '$Fillimi2017' and '$Mbarimi2017' 
group by EmriPartise,Data 
order by Data,EmriPartise;";
    $dbResponseView = mysqli_query($dbConn , $query);

}
	$sqlQuery = "select d4.EmriPartise EmriPartise,datediff('2017-06-11',d7.data) Dataa,d4.rate rate2014,d7.rate rate2017,d4.data data2014,d7.data data2017 
				from d2014 d4,d2017 d7 
				where DATE_FORMAT(DATE_ADD(d4.data , INTERVAL 5 DAY), '%m-%d') = DATE_FORMAT(d7.data, '%m-%d')";
	$dbResponse = mysqli_query($dbConn , $sqlQuery);
	if (!$dbResponse)
	{
		die('Database couldnt be reached: ' . $dbResponse );
	}
	else
	{
		$rows = array();
		while($r = mysqli_fetch_assoc($dbResponse)) {
			$rows[] = $r;
		}
		print json_encode($rows, JSON_PRETTY_PRINT);
	}
}
?>