<?php
/**
 * Created by PhpStorm.
 * User: GentR
 * Date: 03-Apr-17
 * Time: 9:13 AM
 */

# getData.php

header("Content-Type: application/json");
set_time_limit(0);


# db_Connection-initialization
$config = parse_ini_file('../../secure/db_config.ini');
$dbConn = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
if (!$dbConn)
{
    die('Connection to database couldnt be established: '.mysqli_connect_error());
}

$sqlQuery = "select name,fan_count,dateInsert from universitetet p1 where p1.dateInsert=(select max(p2.dateInsert) from universitetet p2 where p2.id=p1.id) order by fan_count desc";
$dbResponse = mysqli_query($dbConn , $sqlQuery);
if (!$dbResponse)
{
    die('Database couldnt be reached: ' . $dbResponse );
}
else
{
    $rows = array();
    while($r = mysqli_fetch_assoc($dbResponse)) {
        $rows[] = $r;
    }
    print json_encode($rows, JSON_PRETTY_PRINT);
}
?>