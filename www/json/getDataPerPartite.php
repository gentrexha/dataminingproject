<?php
/**
 * Created by PhpStorm.
 * User: GentR
 * Date: 03-Apr-17
 * Time: 9:13 AM
 */

# getData.php

header("Content-Type: application/json");

# db_Connection-initialization
$config = parse_ini_file('../../secure/db_config.ini');
$dbConn = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
//$dbConn = mysqli_connect('localhost','root','','maca');

if (!$dbConn)
{
    die('Connection to database couldnt be established: '.mysqli_connect_error());
}
$sqlQuery = "select koalicionet,nrKomente*0.3196+nrLike*0.2896+nrShare*0.3908 as Rate from postimetepartive group by koalicionet";

//$sqlQuery = "select koalicionet,sum(nrkomenteve)+sum(nrlike)+sum(nrshare) as Rate from postimet group by koalicionet";
$dbResponse = mysqli_query($dbConn , $sqlQuery);
if (!$dbResponse)
{
    die('Database couldnt be reached: ' . $dbResponse );
}
else
{
    $rows = array();
    while($r = mysqli_fetch_assoc($dbResponse)) {
        $rows[] = $r;
    }
    print json_encode($rows, JSON_PRETTY_PRINT);
}

?>


