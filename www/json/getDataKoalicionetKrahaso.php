<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 6/2/2017
 * Time: 14:09
 */
header("Content-Type: application/json");

# db_Connection-initialization
$config = parse_ini_file('../../secure/db_config.ini');
$dbConn = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
if (!$dbConn)
{
    die('Connection to database couldnt be established: '.mysqli_connect_error());
}


$PartiaEmri="";
if(isset($_REQUEST["t"])) {

    $Emri = strval($_REQUEST["t"]);

    $vlerat = explode(";", $Emri);
    $Menyra = mysqli_real_escape_string($dbConn,$vlerat[0]);
    $Fillimi = mysqli_real_escape_string($dbConn,$vlerat[1]);
    $Mbarimi = mysqli_real_escape_string($dbConn,$vlerat[2]);
    $EmriKandidatit = mysqli_real_escape_string($dbConn,$vlerat[3]);


    $sqlQuery = "select EmriKoalicionit,Data,sum($Menyra) rate from datarrethkoalicioneve
WHERE EmriKoalicionit='$EmriKandidatit' and data BETWEEN '$Fillimi' AND '$Mbarimi'
group by EmriKoalicionit,Data
order by Data,EmriKoalicionit asc";
    if($Menyra=="TEGJITHA")
        $sqlQuery = "select EmriKoalicionit,Data,nrKomente*0.3196+nrLike*0.2896+nrShare*0.3908 rate from datarrethkoalicioneve
WHERE EmriKoalicionit='$EmriKandidatit' and data BETWEEN '$Fillimi' AND '$Mbarimi'
group by EmriKoalicionit,Data
order by Data,EmriKoalicionit asc";
    //echo $sqlQuery;

    $dbResponse = mysqli_query($dbConn , $sqlQuery);
    if (!$dbResponse)
    {
        die('Database couldnt be reached: ' . $dbResponse );
    }
    else
    {
        $rows = array();
        while($r = mysqli_fetch_assoc($dbResponse)) {
            $rows[] = $r;
        }
        print json_encode($rows, JSON_PRETTY_PRINT);
    }
}
?>