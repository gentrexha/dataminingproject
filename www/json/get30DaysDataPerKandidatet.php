<?php
/**
 * Created by PhpStorm.
 * User: GentR
 * Date: 03-Apr-17
 * Time: 9:13 AM
 */

# getData.php

header("Content-Type: application/json");

# db_Connection-initialization
$config = parse_ini_file('../../secure/db_config.ini');
$dbConn = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
if (!$dbConn)
{
    die('Connection to database couldnt be established: '.mysqli_connect_error());
}

//$sqlQuery = "SELECT DATE_FORMAT(m1, '%Y-%m-%d') data FROM ( SELECT SUBDATE( NOW() , INTERVAL 31 DAY) + INTERVAL m DAY AS m1 FROM ( select @rownum:=@rownum+1 as m from (select 1 union select 2 union select 3 union select 4) t1, (select 1 union select 2 union select 3 union select 4) t2, (select 1 union select 2 union select 3 union select 4) t3, (select 1 union select 2 union select 3 union select 4) t4, (select @rownum:=-1) t0 ) d1 ) d2 WHERE m1 <= now()- INTERVAL 1 DAY ORDER BY m1 desc";
//$dbResponse = mysqli_query($dbConn , $sqlQuery);
//
//    while($row = mysqli_fetch_assoc($dbResponse)) {
//        $muajiFundit[] =$row["data"];
//	}
//
//
//foreach($muajiFundit as $dita)
//{
//	$sqlQuery1 = "select kandidatiname,date(kohapostimit) dataa ,sum(nrkomenteve) komentee,sum(nrlike) likee ,sum(nrshare) sharee,sum(nrReaction) reaction,count(*) totpost from postimetekandidateve
//WHERE   date(kohapostimit) ='$dita'
//group by kandidatiname,dataa
//UNION
//select DISTINCT kandidatiname,'$dita' as dataa,'0' as komentee,'0' as likee,'0' as sharee,'0' as reaction,'0' as totpost
//FROM   postimetekandidateve
//WHERE kandidatiname NOT IN (
//select kandidatiname from postimetekandidateve WHERE   date(kohapostimit) = '$dita' group by kandidatiname)";
//	$dbResponse1 = mysqli_query($dbConn , $sqlQuery1);
//
//	while($row1 = mysqli_fetch_assoc($dbResponse1)) {
//        $emri=$row1["kandidatiname"];
//		$data=$row1["dataa"];
//		$nrLike=$row1["likee"];
//        $nrReaction=$row1["reaction"];
//        $nrKomente=$row1["komentee"];
//        $nrShare=$row1["sharee"];
//        $nrPosteve=$row1["totpost"];
//
//		$sql = "INSERT INTO datarrethkandidateve (EmriKandidatit, Data, nrLike,nrReaction,nrKomente,nrShare,nrPosteve) VALUES ('$emri', '$data', '$nrLike', '$nrReaction', '$nrKomente', '$nrShare', '$nrPosteve')ON DUPLICATE KEY UPDATE
//			Data='$data',nrLike='$nrLike',nrReaction='$nrReaction',nrKomente='$nrKomente',nrShare='$nrShare',nrPosteve='$nrPosteve'";
//
//		mysqli_query($dbConn, $sql);
//
//
//
//}
//}

$sqlQuery = "select EmriKandidatit,data,nrKomente*0.3196+nrLike*0.2896+nrShare*0.3908 rate from datarrethkandidateve
WHERE   data BETWEEN CURDATE() - INTERVAL 31 DAY AND CURDATE() - INTERVAL 1 DAY
group by EmriKandidatit,data
order by data,EmriKandidatit asc";
$dbResponse = mysqli_query($dbConn , $sqlQuery);
if (!$dbResponse)
{
    die('Database couldnt be reached: ' . $dbResponse );
}
else
{
    $rows = array();
    while($r = mysqli_fetch_assoc($dbResponse)) {
        $rows[] = $r;
    }
    print json_encode($rows, JSON_PRETTY_PRINT);
}

?>