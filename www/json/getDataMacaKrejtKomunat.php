<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 6/1/2017
 * Time: 18:43
 */


header("Content-Type: application/json");

# db_Connection-initialization
$config = parse_ini_file('../../secure/db_config.ini');
$dbConn = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
if (!$dbConn)
{
    die('Connection to database couldnt be established: '.mysqli_connect_error());
}


if(isset($_REQUEST["t"])) {

    $Emri=strval($_REQUEST["t"]);

    $vlerat=explode(";",$Emri);
    $Menyra=mysqli_real_escape_string($dbConn,$vlerat[0]);
    $Fillimi=mysqli_real_escape_string($dbConn,$vlerat[1]);
    $Mbarimi=mysqli_real_escape_string($dbConn,$vlerat[2]);
//postimetekryetarevekomunave(postiid,EmriKandidatit,EmriPartise,kohapostimit,nrLike,nrReaction,nrKomenteve,nrShare)

   /* $sqlQuery = "select k.emri,max(k.rate) maksimumi from (select CONCAT(EmriKandidatit,';',Partia) emri,data,sum(nrKomenteve)*0.385+sum(nrLike)*0.235+sum(nrShare)*0.38 rate from datarrethkryetarevekomunave
WHERE   data BETWEEN '$Fillimi' AND '$Mbarimi'
group by emri,data
order by data,emri asc) k"; */

$sqlQuery="select k.emri emri,k.komuna komuna,k.rate maksimumi,k.rate1 max
from (select CONCAT(EmriKandidatit,';',Partia) emri,komuna,data,CONCAT('Like:',nrLike,', Komente:',nrKomenteve,', Share:',nrShare) rate, nrKomenteve*0.3196+nrLike*0.2896+nrShare*0.3908 rate1
FROM `datarrethkryetarevekomunave`
WHERE data='$Fillimi' 
group by EmriKandidatit 
ORDER BY `komuna` ASC,nrKomenteve*0.3196+nrLike*0.2896+nrShare*0.3908 desc) k
group by k.komuna order by k.komuna";

    $dbResponse = mysqli_query($dbConn, $sqlQuery);
    if (!$dbResponse) {
        die('Database couldnt be reached: ' . $dbResponse);
    } else {
        $rows = array();
        while ($r = mysqli_fetch_assoc($dbResponse)) {
            $rows[] = $r;
        }
        print json_encode($rows, JSON_PRETTY_PRINT);
    }
}
?>