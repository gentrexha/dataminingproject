<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 6/1/2017
 * Time: 18:43
 */


header("Content-Type: application/json");

# db_Connection-initialization
$config = parse_ini_file('../../secure/db_config.ini');
$dbConn = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
if (!$dbConn)
{
    die('Connection to database couldnt be established: '.mysqli_connect_error());
}


if(isset($_REQUEST["t"])) {

    $Emri=strval($_REQUEST["t"]);

    $vlerat=explode(";",$Emri);
	$Komuna=mysqli_real_escape_string($dbConn,$vlerat[0]);
//postimetekryetarevekomunave(postiid,EmriKandidatit,EmriPartise,kohapostimit,nrLike,nrReaction,nrKomenteve,nrShare)
    $sqlQuery = "select CONCAT(d.EmriKandidatit,' (',Partia,')') as emri, l.totali
from (select EmriKandidatit,count(*) as totali
from (select EmriKandidatit
from datarrethkryetarevekomunave
WHERE Komuna='$Komuna'
group by EmriKandidatit) k) l, datarrethkryetarevekomunave d
where Komuna='$Komuna'
GROUP by d.emrikandidatit";

    $dbResponse = mysqli_query($dbConn, $sqlQuery);
    if (!$dbResponse) {
        die('Database couldnt be reached: ' . $dbResponse);
    } else {
        $rows = array();
        while ($r = mysqli_fetch_assoc($dbResponse)) {
            $rows[] = $r;
        }
        print json_encode($rows, JSON_PRETTY_PRINT);
    }
}
?>