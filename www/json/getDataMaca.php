<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 6/1/2017
 * Time: 18:43
 */


header("Content-Type: application/json");

# db_Connection-initialization
$config = parse_ini_file('../../secure/db_config.ini');
$dbConn = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
if (!$dbConn)
{
    die('Connection to database couldnt be established: '.mysqli_connect_error());
}


if(isset($_REQUEST["t"])) {

    $Emri=strval($_REQUEST["t"]);

    $vlerat=explode(";",$Emri);
    $Menyra=mysqli_real_escape_string($dbConn,$vlerat[0]);
    $Fillimi=mysqli_real_escape_string($dbConn,$vlerat[1]);
    $Mbarimi=mysqli_real_escape_string($dbConn,$vlerat[2]);
	$Komuna=mysqli_real_escape_string($dbConn,$vlerat[3]);
//postimetekryetarevekomunave(postiid,EmriKandidatit,EmriPartise,kohapostimit,nrLike,nrReaction,nrKomenteve,nrShare)
    $sqlQuery = "select CONCAT(EmriKandidatit,' ',Partia) emri,data,$Menyra rate from datarrethkryetarevekomunave
WHERE   data BETWEEN '$Fillimi' AND '$Mbarimi' and Komuna='$Komuna'
group by emri,data
order by data,emri asc";
if($Menyra=="TEGJITHA")
    $sqlQuery = "select CONCAT(EmriKandidatit,' ',Partia) emri,data,nrKomenteve*0.3196+nrLike*0.2896+nrShare*0.3908 rate from datarrethkryetarevekomunave
WHERE   data BETWEEN '$Fillimi' AND '$Mbarimi' and Komuna='$Komuna'
group by emri,data
order by data,emri asc";

    $dbResponse = mysqli_query($dbConn, $sqlQuery);
    if (!$dbResponse) {
        die('Database couldnt be reached: ' . $dbResponse);
    } else {
        $rows = array();
        while ($r = mysqli_fetch_assoc($dbResponse)) {
            $rows[] = $r;
        }
        print json_encode($rows, JSON_PRETTY_PRINT);
    }
}
?>